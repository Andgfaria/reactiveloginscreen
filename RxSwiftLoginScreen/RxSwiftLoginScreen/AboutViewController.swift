//
//  AboutViewController.swift
//  RxSwiftLoginScreen
//
//  Created by André Gimenez Faria on 25/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var profileImageView: ProfileImageView!
    
    @IBOutlet weak var infoTextView: UITextView!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoTextView.linkTextAttributes = [ NSForegroundColorAttributeName : self.navigationController?.navigationBar.barTintColor ?? UIColor.whiteColor()]
    }
    
}
