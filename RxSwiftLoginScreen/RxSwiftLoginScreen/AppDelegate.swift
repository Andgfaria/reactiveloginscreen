//
//  AppDelegate.swift
//  RxSwiftLoginScreen
//
//  Created by André Gimenez Faria on 20/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

