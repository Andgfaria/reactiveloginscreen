//
//  LoginViewController.swift
//  RxSwiftLoginScreen
//
//  Created by André Gimenez Faria on 20/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LoginViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var loginTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    //MARK: - Instance Variables
    
    private let disposableBag = DisposeBag()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
        setupShowPasswordButton()
        setupKeyboardHandling()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        loginTextField.becomeFirstResponder()
    }
    
    //MARK: - Setup methods
    
    private func setupObservers() {
        
        //When the user press the next button after typing the username, the password textfield becomes the first respond
        loginTextField.rx_controlEvent(.EditingDidEndOnExit).subscribeNext {
            self.passwordTextField.becomeFirstResponder()
        }.addDisposableTo(disposableBag)
        
        // The login button is enabled only when both the login text and the password text are not empty
        Observable.combineLatest(loginTextField.rx_text, passwordTextField.rx_text) { loginText, passwordText  in
            return !loginText.isEmpty && !passwordText.isEmpty
        }.bindTo(loginButton.rx_enabled).addDisposableTo(disposableBag)
        
        // When the user taps the login button we show him a message
        loginButton.rx_tap.subscribeNext {
            self.showLoginMessage()
        }.addDisposableTo(disposableBag)
        
    }
    
    // Create a show password button and insert it as the password textfield right view
    private func setupShowPasswordButton() {
        
        let showPasswordButton = UIButton(frame: CGRectMake(0, 0, 24, 24))
        showPasswordButton.setImage(UIImage(named: "eye"), forState: .Normal)
        
        // Toggle the secure text entry property of the textfield
        showPasswordButton.rx_tap.subscribeNext {
            self.passwordTextField.secureTextEntry = !self.passwordTextField.secureTextEntry
        }.addDisposableTo(disposableBag)
        
        let buttonView = UIView(frame: CGRectMake(0,0,48,24))
        buttonView.addSubview(showPasswordButton)
        showPasswordButton.center = buttonView.center
        
        passwordTextField.rightView = buttonView
        passwordTextField.rightViewMode = .Always
        
    }
    
    private func setupKeyboardHandling() {
    
        // Change the scroll view constraint constant according to the keyboard height
        NSNotificationCenter.defaultCenter().rx_notification(UIKeyboardWillShowNotification).map {
            ($0.userInfo?[UIKeyboardFrameEndUserInfoKey]?.CGRectValue() ?? CGRectZero).height
        }.subscribeNext {
            self.scrollViewBottomConstraint.constant = $0
            self.view.layoutIfNeeded()
        }.addDisposableTo(disposableBag)
        
        // Reset the scroll view constraint
        NSNotificationCenter.defaultCenter().rx_notification(UIKeyboardWillHideNotification).subscribeNext {_ in
            self.scrollViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }.addDisposableTo(disposableBag)
        
    }
    
    //MARK: - Actions
    
    private func showLoginMessage() {
        let alertController = UIAlertController(title:"Login Successfull", message:nil, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }

}
