//
//  ProfileImageView.swift
//  RxSwiftLoginScreen
//
//  Created by André Gimenez Faria on 25/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import UIKit

@IBDesignable class ProfileImageView: UIImageView {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.layer.borderWidth = 6.0
    }

}
