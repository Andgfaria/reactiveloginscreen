//
//  SimpleRoundedButton.swift
//  RxSwiftLoginScreen
//
//  Created by André Gimenez Faria on 20/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import UIKit

@IBDesignable class SimpleRoundedButton: UIButton {

    @IBInspectable var radius : CGFloat = 0.0
    
    // Make the button background transparent when not enabled
    override var enabled: Bool {
        didSet {
            self.backgroundColor = self.backgroundColor?.colorWithAlphaComponent(enabled ? 1.0 : 0.25)
        }
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
}
