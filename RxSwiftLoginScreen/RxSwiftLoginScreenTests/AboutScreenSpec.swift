//
//  AboutScreenSpec.swift
//  RxSwiftLoginScreen
//
//  Created by André Gimenez Faria on 25/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import XCTest
import Quick
import Nimble
@testable import RxSwiftLoginScreen

class AboutScreenSpec: QuickSpec {
    
    override func spec() {
        
        describe("The about screen") {
            
            var aboutController : AboutViewController!
            
            beforeEach {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle(forClass: AboutViewController.self))
                aboutController = mainStoryboard.instantiateViewControllerWithIdentifier("AboutViewController") as! AboutViewController
                _ = aboutController.view
            }
            
            it("has an image view with a picture of me") {
                expect(aboutController.profileImageView.image) == UIImage(named: "me")
            }
            
            it ("has an text view with 3 links") {
                
                let string = aboutController.infoTextView.attributedText.string
                let dataTypes : NSTextCheckingType = [.Link, .PhoneNumber]
                let dataDetector = try! NSDataDetector(types: dataTypes.rawValue)
                
                var links : [NSTextCheckingResult] = []
                dataDetector.enumerateMatchesInString(string, options: [], range: NSMakeRange(0,(string as NSString).length), usingBlock: { (result, flags, _) in
                    links.append(result!)
                })
                
                if(links.count == 3) {
                    expect(links[0].phoneNumber) == "+55 11 99878-8481"
                    expect(links[1].URL!.absoluteString) == "mailto:andfaria13@gmail.com"
                    expect(links[2].URL!.absoluteString) == "https://github.com/andgfaria"
                }
                else {
                    fail()
                }
                
            }
            
            it("has an text view with links enabled") {
                let infoTextView = aboutController.infoTextView
                expect(infoTextView.selectable) == true
                expect(infoTextView.editable) == false
                expect(infoTextView.dataDetectorTypes.rawValue) == 3
                expect(infoTextView.userInteractionEnabled) == true
            }
            
            
            
        }
        
    }
    
}
