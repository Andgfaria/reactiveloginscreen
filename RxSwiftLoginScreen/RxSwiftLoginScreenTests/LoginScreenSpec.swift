//
//  LoginScreenSpec.swift
//  RxSwiftLoginScreen
//
//  Created by André Gimenez Faria on 21/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import Quick
import Nimble
@testable import RxSwiftLoginScreen

class LoginScreenSpec: QuickSpec {
    
    override func spec() {
        
        describe("The login screen") {
            
            var loginController : LoginViewController!
            
            beforeEach {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle(forClass: LoginViewController.self))
                loginController = mainStoryboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
                _ = loginController.view
            }
            
            it("enables the login button when the username and password texts are not empty") {
                dispatch_async(dispatch_get_main_queue(), {
                    loginController.loginTextField.text = "André"
                    loginController.passwordTextField.text = "123"
                    expect(loginController.loginButton.enabled) == true
                })
            }
            
            it("disables the login button when one of the input texts is empty") {
                dispatch_async(dispatch_get_main_queue(), {
                    loginController.loginTextField.text = "André"
                    expect(loginController.loginButton.enabled) == false
                })
            }
            
            it("toggles the password textfield secure text entry") {
                let toggleButton = loginController.passwordTextField.rightView!.subviews[0] as! UIButton
                toggleButton.sendActionsForControlEvents(.TouchUpInside)
                expect(loginController.passwordTextField.secureTextEntry) == false
                toggleButton.sendActionsForControlEvents(.TouchUpInside)
                expect(loginController.passwordTextField.secureTextEntry) == true
            }
            
        }
        
    }
    
}
