//
//  AboutScreenUITests.swift
//  RxSwiftLoginScreen
//
//  Created by André Gimenez Faria on 25/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import XCTest

class AboutScreenUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIDevice.sharedDevice().orientation = .Portrait
        XCUIApplication().launch()
    }
    
    func testUIComponents() {
        
        let app = XCUIApplication()
        
        app.navigationBars["Login"].buttons["About"].tap()
        
        XCTAssert(app.images["Developer Picture"].exists)
        XCTAssert(app.textViews["Info Text View"].exists)
        
    }
    
}
