//
//  RxSwiftLoginScreenUITests.swift
//  RxSwiftLoginScreenUITests
//
//  Created by André Gimenez Faria on 22/07/16.
//  Copyright © 2016 Andre. All rights reserved.
//

import XCTest

class LoginScreenUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIDevice.sharedDevice().orientation = .Portrait
        XCUIApplication().launch()
    }
    
    func testLoginInput() {
        
        let app = XCUIApplication()
    
        app.textFields["Username"].typeText("André")
        app.textFields["Username"].typeText("\n")
        
        app.buttons["Login"].tap()
        
        app.secureTextFields["Password"].typeText("123")
        app.secureTextFields["Password"].typeText("\n")
        
        app.buttons["Login"].tap()
        
        
        app.alerts["Login Successfull"].collectionViews.buttons["OK"].tap()
        
    }
    
}
